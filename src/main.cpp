#include <windows.h>

#include "TSFuncs.hpp"
#include <list>

struct draw_stmt
{
	char* shapename;
	int x, y;
	float cR,cG,cB,cA;
};

std::list<draw_stmt> draw_stmts;

BlFunctionDef(void, __thiscall, GuiShapeNameHud__drawName, ADDR, char*, ADDR, int, int, float, float, float, float);
BlFunctionHookDef(GuiShapeNameHud__drawName);

void __fastcall GuiShapeNameHud__drawNameHook(ADDR obj, void *blank,
	char* shapename,
	ADDR argC,
	int x, int y,
	float cR, float cG, float cB, float cA)
{
	//unknown what argC does - always equal to 1.0f in my brief analysis

	draw_stmt stmt;
	stmt.shapename = shapename;
	stmt.x = x;
	stmt.y = y;
	stmt.cR = cR;
	stmt.cG = cG;
	stmt.cB = cB;
	stmt.cA = cA;
	draw_stmts.push_back(stmt);
		
	GuiShapeNameHud__drawNameOriginal(obj, shapename, argC, x, y, cR, cG, cB, cA);
}

BlFunctionDef(void, __thiscall, GuiShapeNameHud__onRender, ADDR, ADDR, ADDR, ADDR);
BlFunctionHookDef(GuiShapeNameHud__onRender);

void __fastcall GuiShapeNameHud__onRenderHook(ADDR obj, void *blank, ADDR argA, ADDR argB, ADDR argC)
{
	draw_stmts.clear();

	GuiShapeNameHud__onRenderOriginal(obj, argA, argB, argC);

	int idx = 0;

	char size_buffer[32];
	itoa(draw_stmts.size(), size_buffer, 10);
	tsf_SetDataField(obj, "meta_snDataCount", NULL, size_buffer);

	for(std::list<draw_stmt>::iterator it = draw_stmts.begin(); it != draw_stmts.end(); ++it)
	{
		char idx_buffer[32];
		itoa(idx, idx_buffer, 10);


		char gen_buffer[32];

		tsf_SetDataField(obj, "meta_snData_name", idx_buffer, it->shapename);

		itoa(it->x, gen_buffer, 10);
		tsf_SetDataField(obj, "meta_snData_x", idx_buffer, gen_buffer);

		itoa(it->y, gen_buffer, 10);
		tsf_SetDataField(obj, "meta_snData_y", idx_buffer, gen_buffer);

		itoa(it->cR * 255, gen_buffer, 10);
		tsf_SetDataField(obj, "meta_snData_cR", idx_buffer, gen_buffer);

		itoa(it->cG * 255, gen_buffer, 10);
		tsf_SetDataField(obj, "meta_snData_cG", idx_buffer, gen_buffer);

		itoa(it->cB * 255, gen_buffer, 10);
		tsf_SetDataField(obj, "meta_snData_cB", idx_buffer, gen_buffer);

		itoa(it->cA * 255, gen_buffer, 10);
		tsf_SetDataField(obj, "meta_snData_cA", idx_buffer, gen_buffer);

		//BlPrintf("newSHAPENAME TEXT: %s", it->shapename);
		//BlPrintf("newSHAPENAME TEXT: %s", it->shapename);
		//BlPrintf("newSHAPENAME X,Y: %d,%d      RGBa: %f,%f,%f,%f", it->x, it->y, it->cR, it->cG, it->cB, it->cA);
		//BlPrintf("---");
		idx++;
	}
}


bool init()
{
	BlInit;

	if (!tsf_InitInternal())
		return false;

	//0x475eb0	GuiShapeNameHud::drawName()
	BlScanFunctionHex(GuiShapeNameHud__drawName, "83 EC 14 53 8B D9");
	BlCreateHook(GuiShapeNameHud__drawName);
	BlTestEnableHook(GuiShapeNameHud__drawName);

	//0x4758d0	GuiShapeNameHud::onRender()
	BlScanFunctionHex(GuiShapeNameHud__onRender, "55 8b ec 83 e4 f8 81 ec 2c 01 00 00 a1 ? ? ? ? 33 c4 89 84 24 28 01 00 00 53 8b d9 56 57");
	BlCreateHook(GuiShapeNameHud__onRender);
	BlTestEnableHook(GuiShapeNameHud__onRender);

	BlPrintf("%s (v%s-%s): init'd", PROJECT_NAME, PROJECT_VERSION, TSFUNCS_DEBUG ? "debug":"release");

	return true;
}

bool deinit()
{
	BlTestDisableHook(GuiShapeNameHud__drawName);
	BlPrintf("%s: deinit'd", PROJECT_NAME);

	return true;
}

bool __stdcall DllMain(HINSTANCE hinstance, int reason, void *reserved)
{
	switch (reason) {
		case DLL_PROCESS_ATTACH:
			return init();
		case DLL_PROCESS_DETACH:
			return deinit();
		default:
			return true;
	}
}

extern "C" void __declspec(dllexport) __cdecl PROJECT_EXPORT(){}
