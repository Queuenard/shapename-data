# Shapename Data

This DLL records currently-drawn shapename data inside the PlayGui_ShapeNameHud object.

I use this in conjunction with my "Screenshot Data" data available at:
https://blocklandglass.com/addons/addon.php?id=1501

When I take a screenshot with this DLL and that addon, it records all the shapenames visible in a file along with other data.

Inside the PlayGui_ShapeNameHud object will be the following variables:  
	meta_snDataCount	Total shapename count  
	meta_snData_name[%i]	Shapename #i text  
	meta_snData_x[%i]	Shapename #i X coordinate (image coordinate)  
	meta_snData_y[%i]	Shapename #i Y coordinate (image coordinate)  
	meta_snData_cR[%i]	Shapename #i color red (0 to 255)  
	meta_snData_cG[%i]	Shapename #i color green (0 to 255)  
	meta_snData_cB[%i]	Shapename #i color blue (0 to 255)  
	meta_snData_cA[%i]	Shapename #i color alpha (0 to 255)  

